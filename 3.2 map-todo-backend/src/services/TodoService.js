import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3000/todos'

// this shows how you van provide a service as class
// an object literal syntax could also have been used

// axios is used here with 'then' callbacks, async/await syntax could also have been used

export default class TodoService {
  get () {
    return axios.get(RESOURCE_PATH).then(result => result.data)
  }
  getById (id) {
    return axios.get(RESOURCE_PATH + '/' + id).then(result => result.data)
  }
  delete (todo) {
    return axios.delete(RESOURCE_PATH + '/' + todo.id)
  }
  add (todo) {
    return axios.post(RESOURCE_PATH, todo).then(result => result.data)
  }
  update (todo) {
    return axios.put(RESOURCE_PATH + '/' + todo.id, todo)
  }
}
